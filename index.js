db.rooms.insertOne(
		{
			"name" : "single",
			"acommodates" : 2,
			"price" : 1000,
			"description" : "A simple room with all the basic necessities",
			"rooms_available" : 10,
			"isAvailable" : false
		}

	);

// Insert Many Method

db.rooms.insertMany([
	{
		"name" : "double",
		"acommodates" : 3,
		"price" : 2000,
		"description" : "A room fit for a small family going on vacation",
		"rooms_available" : 5,
		"isAvailable" : false

	},

	{
		"name" : "queen",
		"acommodates" : 4,
		"price" : 4000,
		"description" : "A room with a queen sized bed perfect for a simple getaway.",
		"rooms_available" : 15,
		"isAvailable" : false
	}


	]);

// findMethod
db.rooms.find({
	"name" : "double"
});

// UpdateMethod
db.rooms.updateOne(
		{
			"name" : "queen"
		},
		{
			$set: {
				"rooms_available" : 0
			}
		}

	);


db.rooms.deleteMany(
		{
			"rooms_available" : 0
		}

	);

d.rooms.deleteMany({});